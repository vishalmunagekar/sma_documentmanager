 var Express = require('express');
 var multer = require('multer');
 var bodyParser = require('body-parser');
 var imageUpload = Express();
 var namefile;
 imageUpload.use(bodyParser.json());

 
 var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./images");
    },
    filename: function(req, file, callback) {
        namefile = Date.now() + "_" + file.originalname;
        callback(null, namefile);
        console.log(namefile);
    }
});

var upload = multer({
    storage: Storage
}).array("image", 1);


imageUpload.post("/upload", function(req, res) {
        upload(req, res, function(err) {
            if (err) {
                return res.send("Something went wrong!");
            }
            return res.send({imageUrl:"http://localhost:3000/resources/"+ namefile});
        });
});

imageUpload.post("/newpost", function(req, res) {
        console.log(req.body);
        return res.send({success:"new post sccessfully Posted...."});
});

module.exports = imageUpload;
